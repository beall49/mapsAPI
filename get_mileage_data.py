"""
   https://developers.google.com/maps/documentation/distance-matrix/intro
       ADDRESS MUST BE IN THIS FORMAT
    "1873" + "+" +"Chur" + "+" + "Ct" + "+" + "Manteca" + "+" + "CA"
"""

import re, json
from urllib import urlopen
GOOGLE_KEY="AIzaSyDQAkbdgDxdszmnFxua2ETKvY2167C8idE"  
URL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key={2}"

def get_mileage(origin, destination):
      
    url = URL.format(origin, destination, GOOGLE_KEY)
    html = urlopen(url).read()
    content = json.loads(html)
    miles = distance = minutes = duration = 0

    try:
        distance = content['rows'][0]["elements"][0]["distance"]["value"]
        duration = content['rows'][0]["elements"][0]["duration"]["value"]
        miles = round((distance/1609.34),1)
        minutes = duration/60
    except:
        print content, url

    return int(miles), minutes, url