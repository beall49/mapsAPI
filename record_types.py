"""
    Keeping handy to print out all attr
    print ', '.join("%s: %s" % item for item in vars(insert_record).items())
"""

class InsertRecord:
    """
        just a record object for organization of the insert statement
        insert object = origin, destination, miles, minutes, route_id, stop_number, url
    """

    def __init__(self, **kwargs):
        self.origin = kwargs['origin']
        self.destination = kwargs['destination']
        self.miles = kwargs['miles']
        self.minutes = kwargs['minutes']
        self.route_id = kwargs['route_id']
        self.stop_num = kwargs['stop_num']
        self.url = kwargs['url']

class SelectRecord:
    """
        just a record object for organization of the selected record set
        select object = stopnum, route_id, template_id, store_name, store_address
    """
    pass

#self.db_conn_string = 'SERVER=seraph;DATABASE=prod;User Id=edi_python;Password=AYqfUkr2R1Z3Rwntycgaq017J;'
class Config: 
    def __init__(self):
        self.company = 'NUC'        
        self.aos_conn_string = 'DEV@LOGOS:2712'
        self.axc_file = 'C:\\Program Files (x86)\Microsoft Dynamics AX\40\Client\Bin\bc_config.axc'
        self.ax_domain = 'nulaid.pvt'
        self.ax_logon = 'svc_ax-bcproxy'        
        self.ax_domain_password = '321qazxc'
        self.ax_lang = 'en-us'