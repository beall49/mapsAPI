﻿using System;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
//C:\Windows\Microsoft.NET\Framework64\v2.0.50727>regasm.exe MapsAPI.dll <= register localy

namespace ncAxMapAPI {
    public class MapsAPI {
        private string NUCAL_HOME = "720+S+Stockton+Ave+Ripon+CA+95366",
                       GOOGLE_KEY = "AIzaSyDQAkbdgDxdszmnFxua2ETKvY2167C8idE";

        /*
            hit api and return deseralized json object
        */
        public MileageResult getMapData(string url) {
            string content = "";
            WebResponse response = WebRequest.Create(url).GetResponse();

            using (Stream stream = response.GetResponseStream()) {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                content = reader.ReadToEnd();
            }

            /*
                push to shared objects class to decode the json
            */
            return JsonConvert.DeserializeObject<MileageResult>(content);
        }

        /*
            create a list of route records
        */
        public List<RouteRecord> getRouteData(string _route_id) {
            List<RouteRecord> routeList = new List<RouteRecord>();
            RouteRecord routeRecord;

            List<RouteRecord> results = getQueryRestults(_route_id);
            for (int i = 0; i < results.Count; i++) {
                routeRecord = new RouteRecord();

                //first record starts at home 
                //(going to have to change later)
                //to first stop
                if (i == 0) {
                    routeRecord.origin_address = NUCAL_HOME;
                    routeRecord.destination_address = results[i].origin_address;
                } else {
                    routeRecord.origin_address = cleanAddress(results[i].origin_address);

                    //if I'm the last record - distance back home
                    if (i == results.Count - 1) {
                        routeRecord.destination_address = NUCAL_HOME;
                    } else {
                        //else get the next address
                        routeRecord.destination_address = results[i + 1].origin_address;
                    }
                }

                string url = getURL(routeRecord.origin_address, cleanAddress(routeRecord.destination_address));
                MileageResult mileageResult = getMapData(url);
                /*
                    if there's a record
                */
                if (mileageResult.status.ToUpper() == "OK") {
                    routeRecord.route_Id = _route_id;
                    routeRecord.stop_num = results[i].stop_num;
                    routeRecord.mileage = mileageResult.rows[0].elements[0].distance.value;
                    routeRecord.minutes = mileageResult.rows[0].elements[0].duration.value;
                    routeList.Add(routeRecord);
                }
            }
            return routeList;
        }


        /*
            Trivials; qry, string manipulation, build url
        */
        public string getURL(string _origin, string _destination) {
            string URL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key={2}";
            return String.Format(URL, _origin, _destination, GOOGLE_KEY);
        }

        private string cleanAddress(string inputString) {
            string REGEX = "[^a-zA-Z0-9 ]";
            return string.Join("+", (Regex.Replace(inputString, REGEX, " ")).Split(' '));
        }

        public List<RouteRecord> getQueryRestults(string _routeId,  string company = "nuc") {
            GetRoutDataDataContext context = new GetRoutDataDataContext();
            List<RouteRecord> results =
                          (from line in context.ASIDLVROUTELINEs
                           join sales in context.SALESTABLEs on
						          new { line.DATAAREAID, line.SALESID } equals
						          new { sales.DATAAREAID, sales.SALESID }
				           where
						           line.DATAAREAID == sales.DATAAREAID &&
						           line.SALESID == sales.SALESID &&
                                   line.DATAAREAID == company &&
                                   line.DLVROUTEID == _routeId &&
                                   line.NOORDER == 0
                           select new RouteRecord {
                               stop_num = line.STOPNUM,
                               origin_address = sales.DELIVERYADDRESS
                           }).OrderBy(x => x.stop_num).ToList();

            return results;
        }
    }
}