"""
   https://developers.google.com/maps/documentation/distance-matrix/intro
       ADDRESS MUST BE IN THIS FORMAT
    "1873" + "+", "Chur" + "+", "Ct" + "+", "Manteca" + "+", "CA"
"""
import re
import json, pprint, get_server_data
from collections import  namedtuple


def get_mileage(origin, destination):
    GOOGLE_KEY="KEY HERE"
    from urllib import urlopen
    url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key{2}&units=imperial".format(origin, destination, GOOGLE_KEY)
    html = urlopen(url).read()
    content = json.loads(html)
    miles, minutes, distance, duration  = 0, 0, 0, 0

    try:
        distance = content['rows'][0]["elements"][0]["distance"]["value"]
        duration = content['rows'][0]["elements"][0]["duration"]["value"]
        miles = round((distance/1609.34),1)
        minutes = duration/60
    except:
        print content, url

    return miles, minutes

def create_entries():
    RouteStop = namedtuple('RouteStop', 'origin, destination, miles, minutes, route_id, stop_number')
    nucal_address = "720+S+Stockton+Ave+Ripon+CA+95366"
    record_Set = sorted(get_server_data.get_open_orders())
    remove_non_alpha = re.compile("[^a-zA-Z0-9 ]")
    origin = destination = ""
    Stops = []



    for tbl in record_Set:
        origin = destination = ""
        route_id = tbl[0][0]
        tbl.sort()
        for index, row in enumerate(tbl):
            origin = destination
            if index == 0:
                origin = nucal_address
                destination = ('+'.join(re.sub(remove_non_alpha, " ", row[2]).split()))
            elif index == len(tbl)-1:
                destination = nucal_address
            else:
                destination = ('+'.join( re.sub(remove_non_alpha, " ", tbl[index+1][2]).split()  ))

            miles, minutes  = get_mileage(origin, destination)
            Stops.append(RouteStop(origin, destination, miles, minutes, route_id, row[1]))


    return Stops
