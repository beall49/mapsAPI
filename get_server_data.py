import pyodbc
def get_open_orders():
    cur = pyodbc.connect('DRIVER={SQL Server};SERVER=servername;DATABASE=dbname;').cursor()
    routes, sub_routes = [], []
    company = 'nuc'

    qry = """
    SELECT
        STOPNUM,
        T.DLVROUTEID                                 AS ROUTEID,
        T.DLVROUTETEMPLATEID                         AS TEMPLATEID,
        replace(ST.ASISTORENAME, '''', '')           AS STORENAME,
        replace(ST.DELIVERYADDRESS, '''', '')        AS STOREADDRESS
    FROM
        ASIDLVROUTETABLE T
    INNER JOIN
        ASIDLVROUTELINE ASIROUTE ON
        ASIROUTE.DLVROUTEID = T.DLVROUTEID AND
        ASIROUTE.DATAAREAID = T.DATAAREAID
    INNER JOIN
        SALESTABLE ST ON
        ST.SALESID    = ASIROUTE.SALESID AND
        ST.DATAAREAID = ASIROUTE.DATAAREAID
    WHERE
        ASIROUTE.DATAAREAID = '{0}'AND
        ASIROUTE.NOORDER = 0 AND
        T.DLVROUTESTATUS IN (0,2) AND
        T.COMPLETIONDATE > GETDATE() -1
    GROUP      BY
        STOPNUM,
        T.DLVROUTEID,
        T.DLVROUTETEMPLATEID,
        ST.ASISTORENAME,
        ST.DELIVERYADDRESS
    ORDER      BY

        3,
        2
        """

    cursor =cur.execute(qry.format(company))
    columns = [column[0] for column in cursor.description]
    tbl=cursor.fetchall()
    cur.close()
    route_id = tbl[0][1]
    for row in tbl:
        if row[1] != route_id:
            routes.append(sub_routes)
            sub_routes = []
            route_id = row[1]
        sub_routes.append([row[1],row[0],row[4],row[3], row[2]])
    return routes


