import clr, sys, re, types
import record_types
from get_open_routes import get_routes as tbl
from get_mileage_data import get_mileage as mileage
REGEX =  re.compile("[^a-zA-Z0-9 ]")
NUCAL_HOME_ADDRESS = "720+S+Stockton+Ave+Ripon+CA+95366"
routes = tbl()
insert_statements = []

for route in routes:
    destination = ""
    route_id = route[0].route_id
    for index, stop in enumerate(route):
        origin = destination

        if index == 0:
            origin = NUCAL_HOME_ADDRESS
            destination = ('+'.join(re.sub(REGEX, " ", stop.store_address).split()))

        elif index == len(route)-1:
            destination = NUCAL_HOME_ADDRESS

        else:
            destination = ('+'.join( re.sub(REGEX, " ", route[index+1].store_address).split()  ))

        miles, minutes, url = mileage(origin, destination)
        
        keywords = {'origin':origin, 'destination': destination, 'miles':miles, 'minutes':minutes, 'route_id':route_id, 'stop_num':stop.stop_num, 'url':url}
        insert_record = record_types.InsertRecord(**keywords)
        insert_statements.append(insert_record)
print insert_statements
print 'done'

#sys.path.append('C:\\Program Files (x86)\\Microsoft Dynamics AX\\40\\Client\\Bin\\')
#clr.AddReference("System")
#clr.AddReference("Microsoft.Dynamics.BusinessConnectorNet")
#from System.Net import NetworkCredential as nc
#from Microsoft.Dynamics import BusinessConnectorNet as bc        

#config = record_types.Config()
#ax = bc.Axapta()

#ax.Logon(None, None, None, None)
#rec = ax.CreateAxaptaRecord("ncRouteMileage")
#rec.ExecuteStmt("select * from %1");
#while rec.Found:
#    print rec.get_Field("URL")
#    rec.Next();
#dictionary = ax.CreateAxaptaObject("Dictionary")
#table_id = dictionary.Call("tableName2Id", "ncRouteMileage")
#table_object = dictionary.Call("tableObject", table_id)
#field_id = table_object.Call("fieldName2Id", "URL")
#print field_id
#ax.Logoff()





