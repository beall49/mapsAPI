﻿using System;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
//cd C:\Windows\Microsoft.NET\Framework\v2.0.50727>regasm.exe MapsAPI.dll <= register localy

namespace ncAxMapAPI {
    public class MapsAPI    {
        private string GOOGLE_KEY="AIzaSyDQAkbdgDxdszmnFxua2ETKvY2167C8idE" ;

        /// <summary>
        ///     heavy lifting; gets data, 
        ///     parses stream and sets values
        /// </summary>            
        public double getMileage(string _origin, string _destination) {    
            this.origin = _origin;
            this.destination = _destination;        
            string content = "";           
            WebResponse response = WebRequest.Create(getURL()).GetResponse();

            using (Stream stream = response.GetResponseStream()) {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                content = reader.ReadToEnd();
            }
            
            MileageResult result = JsonConvert.DeserializeObject<MileageResult>(content);

            if (result.status.ToUpper() == "OK") {
                return result.rows[0].elements[0].distance.value;
            }
            return 0;
        }

        /// <summary>
        /// returns formatted URL
        /// </summary>
        /// <returns>formatted URL (origin, destination, apikey)</returns>
        public string getURL() {
            string URL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key={2}";
            return String.Format(URL, origin, destination, GOOGLE_KEY);
        }

        /// <summary>
        /// cleans address for url encoding
        /// </summary>
        /// <param name="inputString"> address string</param>
        /// <returns>Address formatted e.g. 720+S+Stockton+Ave+Ripon+CA+95366</returns>
        private string cleanAddress(string inputString) {
            string REGEX = "[^a-zA-Z0-9 ]";
            return string.Join("+", (Regex.Replace(inputString, REGEX, " ")).Split(' '));
        }


        private string _destination;
        /// <summary>
        /// destination address - cleaned on set
        /// </summary>
        public string destination {
            get { return _destination; }                        
            set { _destination = cleanAddress(value);}
        }

        private string _origin;
        /// <summary>
        /// origin address - cleaned on set
        /// </summary>
        public string origin {
            get { return _origin; }
            set { _origin = cleanAddress(value);}
        }
    }
}