﻿using System.Collections.Generic;
using System;
namespace ncAxMapAPI {

    public class MileageResult {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<Row> rows { get; set; }
        public string status { get; set; }
    }

    public class Row {
        public List<Element> elements { get; set; }
    }

    public class Element {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
    }

    public class Distance {
        public string text { get; set; }
        private double _values;
        /// <summary>
        /// distance between two points in miles (meters/1609.34)
        /// </summary>
        public double value {
            get { return _values; }
            set { _values = Math.Round((value / 1609.34),1); }
        }
    }

    public class Duration {
        public string text { get; set; }
        private int _value;
        /// <summary>
        /// time between two points in minutes
        /// </summary>
        public int value {
            get { return _value; }
            set { _value = Convert.ToInt32(value / 60); }
        }
    }
}
