﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ncAxMapAPI {
    /*
        just to test with
    */
    class StartTest {
        static void Main(string[] args) {
            MapsAPI maps = new MapsAPI();
            string origin = "720 S Stockton Ave Ripon CA 95366";
            string destination = "1530 Hamilton Ave San Jose, CA 95123";
            Debug.Write(maps.getMileage(origin, destination));
        }
    }
}
