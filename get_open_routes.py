"""
    This is going out to seraph and selecting all open 
    routes for today, ordered by route id, stop number.
    
    It's then creating a list of lists, which help create
    organized sets of data based off the above sort criteria.

    This is done, so we can easily no when to start a new 
    origin and destination address (begining and end of a set of addresses)
"""


import clr, sys
import record_types 

sys.path.append("C:\\projects\\RAP\\Mileage\\MileageAPI\\")
clr.AddReference("Microsoft.Dynamics.BusinessConnectorNet.dll")
clr.AddReference("System.Data")

from sql_statements import select_routes as select
from System.Data.SqlClient import SqlConnection
from Microsoft.Dynamics import BusinessConnectorNet as bc

CONN_STRING = 'SERVER=seraph;DATABASE=prod;User Id=edi_python;Password=AYqfUkr2R1Z3Rwntycgaq017J'


def get_routes(company = 'nuc'):
    conn = SqlConnection(CONN_STRING)
    conn.Open()
    cmd = conn.CreateCommand()
    cmd.CommandText = select(company)
    reader = cmd.ExecuteReader()
    route_id = ''
    routes = stops = []

    while reader.Read():  
        #stopnum, route_id,  store_name, store_address
        if route_id != reader[1]:
            if route_id != '':
                routes.append(stops)
            route_id = reader[1]        
            stops = []

        select_record = record_types.SelectRecord()
        select_record.stop_num = reader[0]
        select_record.route_id = route_id
        select_record.store_name = reader[2]
        select_record.store_address = reader[3]  
        stops.append(select_record)

    conn.Close()
    return routes