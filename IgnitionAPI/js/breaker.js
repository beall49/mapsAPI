app.controller('BreakerController', function($scope, $interval, $http, $sce, API) {
    var myWasteChart = null;
    var mySpeedChart = null;
    var mySpeedValues = [];
    var myDTValues = [];
    $scope.Company = "NULAID";
    $scope.colorState = "primary";
    $scope.Downtime = "";
    hideDownTime(true);
    getWasteData();
    getSpeedData();
    

    $interval(function() {
        getSpeedData();
        getWasteData();
    }, 10000);

    $interval(function() {    
        API.getTagData().success(function(response) {
            if ($scope.Metric != response) {
                $scope.Metric = response;
                $scope.colorState = (response['colorState']);
            
                if (["warning", "danger"].indexOf($scope.colorState) >= 0) {
                    getDownTime();
                } else if ($scope.Downtime != "") {
                    $scope.Downtime = "";
                    hideDownTime(true);
                }
            }
        });

    } , 1000);

    function hideDownTime(bool) {
        if (bool == true) {
            $("#downtime").hide();
        } else {
            $("#downtime").show();
        }
    }

    function getSpeedData() {
        API.getSpeedData().success(function(response) {
            var speedObject = response['rows'];
            
            var myTempValues = [];
            angular.forEach(speedObject, function (item) {
                myTempValues.push({ 'meta': 'Speed', 'value': item.value });
            });

            if (myTempValues != mySpeedValues) {
                var data = {
                    labels: [],
                    series: [myTempValues]
                };
                mySpeedChart = new Chartist.Line('#speedChart', data, {
                    low: 0,
                    plugins: [Chartist.plugins.tooltip()]
                });
                mySpeedValues = myTempValues;
            }
        });
    }

    function getDownTime() {
        API.getDownTimeData().success(function (response) {
            if ($scope.Downtime != response.downtime_reasons) {
                $scope.Downtime = response.downtime_reasons;
                hideDownTime(false);
            }
            API.getTimer().success(function(response) {
                $scope.Timer = response.time;
            });
        });
    }

    function getWasteData() {
        API.getWasteData().success(function (response) {
          
            var data = {
                labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
                series: [
                    [
                        response['rows']['0'].broken,
                        response['rows']["1"].broken,
                        response['rows']["2"].broken,
                        response['rows']["3"].broken,
                        response['rows']["4"].broken,
                        response['rows']["5"].broken,
                        response['rows']["6"].broken,
                        response['rows']["7"].broken,
                        response['rows']["8"].broken,
                        response['rows']["9"].broken,
                        response['rows']["10"].broken,
                        response['rows']["11"].broken
                    ],
                    [
                        response['rows']['0'].waste,
                        response['rows']["1"].waste,
                        response['rows']["2"].waste,
                        response['rows']["3"].waste,
                        response['rows']["4"].waste,
                        response['rows']["5"].waste,
                        response['rows']["6"].waste,
                        response['rows']["7"].waste,
                        response['rows']["8"].waste,
                        response['rows']["9"].waste,
                        response['rows']["10"].waste,
                        response['rows']["11"].waste
                    ]
                ]
            };
            myWasteChart = new Chartist.Bar('#wasteChart', data, {
                stackBars: true,
                plugins: [Chartist.plugins.tooltip()]
            });
        });
    }
    
    API.getNavBar().success(function(response) {            
        $scope.NavBar = $sce.trustAsHtml(response);   
    });
       
    API.getSideBar().success(function(response) {            
        $scope.SideBar = $sce.trustAsHtml(response);   
    });    
});