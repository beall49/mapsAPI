app.controller('AssetsController', function($scope, $interval, $http, $sce, API) {
    $scope.Company = "NULAID";
    $scope.colorState = "primary";
    getAssetData("1");
    getAssetType();
        
    
    $scope.selectedRow = function(rowID) {
        console.log(rowID);
    }
        
    function getAssetData(type) {
        API.getAssetData(type).success(function(response) {
            $scope.Table = response;   
        });
    }
    
    function getAssetType() {
        API.getAssetTypes().success(function(response) {
            $scope.Types = response;   
        });
    }    
       
    API.getNavBar().success(function(response) {            
        $scope.NavBar = $sce.trustAsHtml(response);   
    });
       
    API.getSideBar().success(function(response) {            
        $scope.SideBar = $sce.trustAsHtml(response);   
    });
                   
    $scope.Selection = function(type, selText) {
        getAssetData(type);
        $("#assetSelector").html(selText + '<span class="caret"></span>');
    }  
})
