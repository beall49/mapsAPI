﻿var app = angular
    .module('myApp', [])
    .run(function ($rootScope) {
        $rootScope.Toggle = function () {
            $("#wrapper").toggleClass("toggled");
        };
    })
    .service('API', function($http) {
        var baseAPI = './api/IgnitionInterface.asmx/';
        return {				
            getTagData: function() {
                return  $http.get(baseAPI + 'Tags');
            },
            getWasteData: function() {			
                return $http.get(baseAPI + 'Waste');                
            },
            getSpeedData: function() {
                return $http.get(baseAPI + 'Speed');
            },
            getDownTimeData: function() {
                return $http.get(baseAPI + 'DownTime');
            },
            getTimer: function() {
                return  $http.get(baseAPI + 'Timer');
            },
            getNavBar: function() {
                return  $http.get('static/navbar.html');
            },     
            getSideBar: function() {
                return  $http.get('static/sidebar.html');
            },                
            getAssetData: function(type) {
                if (type == ""){
                    type = "1";
                }
                return  $http.json(baseAPI + 'api/assets/asset_data?category=' + type);
            }    ,
            getAssetTypes: function() {
                return  $http.jsonp(baseAPI + 'api/assets/asset_types');
            }             
        };     
    })
.directive('compileTemplate', function($compile, $parse){
    return {
        link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
            function getStringValue() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
            scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }         
    }
});



    

