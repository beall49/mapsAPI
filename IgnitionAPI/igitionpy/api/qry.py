	qry = """SELECT 
		ID,
		A.CATEGORY_ID,
		A.ASSET_ID,
		ASSET_ENABLED,
		ASSET_DAMAGED,
		C.CATEGORY_TYPE,
		D.ASSET_DESCR,
		D.ASSET_USER,
		D.ASSET_LOC,
		D.ASSET_CO
	FROM
		IGNITION.DBO.TBL_ASSETS A
	INNER JOIN 
		DBO.TBL_CATEGORIES C ON
		A.CATEGORY_ID = C.CATEGORY_ID 
	LEFT JOIN 
		DBO.TBL_ASSET_DESCR D ON
		A.ASSET_ID = D.ASSET_ID
	where
		a.CATEGORY_ID = %s"""
	try:
		parms = request['params']	
		category = parms['category']			
	except:
		category = "1"
		
	if not str(category).isdigit():
		category = "1"
		
	data_set = system.db.runQuery( qry %(category))
	ds = {}
	for index, row in enumerate(data_set):		
		ds[index] = ({'AID': str(row['ASSET_ID']), 
						'INUSE': str(row['ASSET_ENABLED']), 
						'DAMAGED': str(row['ASSET_DAMAGED']),
						'USER': str(row['ASSET_USER']),
						'LOC': str(row['ASSET_LOC']),
						'COMPANY': str(row['ASSET_CO'])})
	
	return {'json': system.util.jsonEncode(dict(sorted(ds.items())),2)}