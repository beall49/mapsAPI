path = "[default]Breaker/Run/DownTimeReasons"
data_set = system.dataset.toPyDataSet(system.tag.read(path).value)
ds = []
for index, row in enumerate(data_set):
	ds.append(row[0])
			
response = { 'json': '{"downtime_reasons":' + system.util.jsonEncode(ds) + '}'}
return response