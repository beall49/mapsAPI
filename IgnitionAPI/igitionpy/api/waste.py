	#	http://nl-ignition:8088/main/system/webdev/DemoProject/history?tagName=Infeed&min=30&returnSize=100
	
	from system.dataset import toDataSet as toDs, toPyDataSet as toPyDs, getColumnHeaders as getHeaders
	headers = ["Cup#", "Waste"]	
	ds = []
	tagNameTypes = "iRowBrokenEggCnt", "iRowFeededEggCnt"
	tagPath = "[default]Breaker/PRD"
	for x in range(1,13):
		brokenEgg = system.tag.getTagValue(tagPath + "/" +tagNameTypes[0] +str(x))
		fedEgg = system.tag.getTagValue(tagPath + "/" +tagNameTypes[1] +str(x))		
		ds.append({ "id": x, "broken": brokenEgg , "waste": (fedEgg-brokenEgg) })
		
	response = { 'json': '{"rows":' + system.util.jsonEncode(ds) + '}'}
	
		
	return response