﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgnitionAPI {

    public class WasteRow {
        public int broken { get; set; }
        public int waste { get; set; }
        public int id { get; set; }
    }

    public class IgnitionWasteObject {
        public List<WasteRow> rows { get; set; }
    }
}