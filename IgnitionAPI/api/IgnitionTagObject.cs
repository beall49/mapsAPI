﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgnitionAPI {
    public class IgnitionTagObject {
        public string waste { get; set; }
        public string outfeed { get; set; }
        public string downtime { get; set; }
        public string infeed { get; set; }
        public string runtime { get; set; }
        public string cat_3 { get; set; }
        public string cip { get; set; }
        public string colorState { get; set; }
        public string state { get; set; }
        public string bad_egg { get; set; }
        public string speed { get; set; }
    }
}