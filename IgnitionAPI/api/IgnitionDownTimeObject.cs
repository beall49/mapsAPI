﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgnitionAPI {
    public class IgnitionDownTimeObject {
        public List<string> downtime_reasons { get; set; }
    }
}