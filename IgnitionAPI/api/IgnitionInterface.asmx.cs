﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Net;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace IgnitionAPI.api {
    /// <summary>
    /// Summary description for IgnitionInterface
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    
    [System.Web.Script.Services.ScriptService]    
    public class IgnitionInterface : System.Web.Services.WebService {
        public string baseURL = "http://nl-ignition:8088/main/system/webdev/DemoProject/";
        private string content = "";

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void  Speed() {          
            string content = getContent(baseURL + "api/speed");        
            IgnitionSpeedObject speed = JsonConvert.DeserializeObject<IgnitionSpeedObject> (content);                       
            HttpContext.Current.Response.Write (new JavaScriptSerializer().Serialize(speed));            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void  Timer() {          
            string content = getContent(baseURL + "api/timer");        
            IgnitionTimerObject timer = JsonConvert.DeserializeObject<IgnitionTimerObject> (content);                       
            HttpContext.Current.Response.Write (new JavaScriptSerializer().Serialize(timer));            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void  Tags() {          
            string content = getContent(baseURL + "api/tags");        
            IgnitionTagObject tags = JsonConvert.DeserializeObject<IgnitionTagObject> (content);                       
            HttpContext.Current.Response.Write (new JavaScriptSerializer().Serialize(tags));            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void  DownTime() {          
            string content = getContent(baseURL + "api/down_time");        
            IgnitionDownTimeObject downtime = JsonConvert.DeserializeObject<IgnitionDownTimeObject> (content);                       
            HttpContext.Current.Response.Write (new JavaScriptSerializer().Serialize(downtime));            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void  Waste() {          
            string content = getContent(baseURL + "api/waste");        
            IgnitionWasteObject waste = JsonConvert.DeserializeObject<IgnitionWasteObject> (content);                       
            HttpContext.Current.Response.Write (new JavaScriptSerializer().Serialize(waste));            
        }
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void  Mileage(string origin, string destination) {
            double mileage = new MapsAPI().getMileage (origin, destination) ;                                                
            HttpContext.Current.Response.Write (new JavaScriptSerializer().Serialize(mileage));            
        }


        public string getContent(string URL) {
            content = "";                         
            using (Stream stream = WebRequest.Create(URL).GetResponse().GetResponseStream()) {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                content = reader.ReadToEnd();
            }

            return content;
        }
        
        
    }
}
