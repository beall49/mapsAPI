	from system.util import jsonEncode as jsonE, jsonDecode as jsonD
	
	
	class Tags:
	    def toJson(self):
	        return jsonD(str(self.__dict__))
	
	
	breaker_path = "Breaker/Run/"
	breaker_prd_path = "Breaker/PRD/"
	
	tags = Tags()
	tags.cip = str(system.tag.read(breaker_path + "CIP").value)
	tags.colorState = str(system.tag.read(breaker_path + "ColorState").value)
	tags.downtime = str(system.tag.read(breaker_path + "DownTime").value)
	tags.infeed = str(system.tag.read(breaker_path + "Infeed").value)
	tags.outfeed = str(system.tag.read(breaker_path + "Outfeed").value)
	tags.runtime = str(system.tag.read(breaker_path + "RunTime").value)
	tags.speed = str(system.tag.read(breaker_path + "Speed").value)
	tags.state = str(system.tag.read(breaker_path + "State").value)
	tags.waste = str(system.tag.read(breaker_path + "waste").value)
	tags.cat_3 = str(system.tag.read(breaker_prd_path + "iCat3EggCnt").value)
	tags.bad_egg = str(system.tag.read(breaker_prd_path + "iBadEggCnt").value)
	
	return {'json': tags.toJson()}