"""
	http://IGNITION:8088/main/system/webdev/PROJECTNAME/FOLDER/ENDPOINT
	
	This would go in your web dev api folders (wherever it fit for you)
	You have to extend the HttpServletResponse object to be able to apply response headers
	otherwise you will run into CORS issues. Basically, built in JS protection against XSS https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
	I've put a copy of that project in the Information_Systems\ignition folder
	The def doGet(request, session): is just  a place holder for formatting purposes, remove that (the end point is arleady doGet)
"""


########get response
"""I put this in a global script to be accessible by all end points"""
def get_response_servlet(request_object):
    from javax.servlet.http import HttpServletResponse  as Response
    Response = request_object
    Response.addHeader('Access-Control-Allow-Origin', '*')
    return Response.getWriter()



##################downtime
def doGet(request, session):
	path = "[default]Breaker/Run/DownTimeReasons"
	data_set = system.dataset.toPyDataSet(system.tag.read(path).value)
	ds = []
	for index, row in enumerate(data_set):
		ds.append(row[0])

	writer = shared.misc.get_response_servlet(request['servletResponse'])
	writer.println('{"rows":' + str(system.util.jsonEncode(ds)) + '}')
	writer.close
	return None

####################sales id
def doGet(request, session):	
	try:
		parms = request['params']
		days_back = parms['days_back']
	except:
		days_back = "300"

	if not days_back.isdigit():
		days_back = "300"

	data_set = system.db.runQuery(
			"select top 10 salesid from salestable where dataareaid = 'nuc' and deliverydate > getdate()-%s" % (
				days_back), "AX_DB")
	ds = {}
	for index, row in enumerate(data_set):
		ds[index] = ({'meta': 'Sales Ids', 'value': int(row[0])})
	return {'json': system.util.jsonEncode(dict(sorted(ds.items())), 2)}

	
	
######################speed
def doGet(request, session):	
	import datetime

	min = 60
	returnSize = 100

	startTime = datetime.datetime.now() - datetime.timedelta(minutes=int(min))
	endTime = datetime.datetime.now()
	dataSet = system.tag.queryTagHistory(
			paths=["[default]Breaker/Run/Speed"],
			startDate=startTime,
			endDate=endTime,
			aggregationMode="SimpleAverage",
			intervalMinutes=5
	)
	pyDataSet = system.dataset.toPyDataSet(dataSet)

	ds = []

	for index, row in enumerate(pyDataSet):
		ds.append({'id': index, 'TimeStamp': str(system.date.format(row[0], "hh:mm:s")),
				   'value': int(row[1])})

	writer = shared.misc.get_response_servlet(request['servletResponse'])
	writer.println('{"rows":' + system.util.jsonEncode(ds) + '}')
	writer.close

######################tags
def doGet(request, session):	
	from system.util import jsonEncode as jsonE, jsonDecode as jsonD

	class Tags:
		def toJson(self):
			return jsonE(jsonD(str(self.__dict__)))

	breaker_path = "Breaker/Run/"
	breaker_prd_path = "Breaker/PRD/"

	tags = Tags()
	tags.cip = str(system.tag.read(breaker_path + "CIP").value)
	tags.colorState = str(system.tag.read(breaker_path + "ColorState").value)
	tags.downtime = str(system.tag.read(breaker_path + "DownTime").value)
	tags.infeed = str(system.tag.read(breaker_path + "Infeed").value)
	tags.outfeed = str(system.tag.read(breaker_path + "Outfeed").value)
	tags.runtime = str(system.tag.read(breaker_path + "RunTime").value)
	tags.speed = str(system.tag.read(breaker_path + "Speed").value)
	tags.state = str(system.tag.read(breaker_path + "State").value)
	tags.waste = str(system.tag.read(breaker_path + "waste").value)
	tags.cat_3 = str(system.tag.read(breaker_prd_path + "iCat3EggCnt").value)
	tags.bad_egg = str(system.tag.read(breaker_prd_path + "iBadEggCnt").value)

	writer = shared.misc.get_response_servlet(request['servletResponse'])
	writer.println(tags.toJson())
	writer.close

#####################timer
def doGet(request, session):	
	path = "[default]Breaker/Run/DownTimeTimer"
	writer = shared.misc.get_response_servlet(request['servletResponse'])
	writer.println('{"time": "' + str(system.tag.read(path).value) + '"}')
	writer.close

#############waste
def doGet(request, session):	
	ds = []
	tagNameTypes = "iRowBrokenEggCnt", "iRowFeededEggCnt"
	tagPath = "[default]Breaker/PRD"

	for x in range(1, 13):
		brokenEgg = system.tag.getTagValue(tagPath + "/" + tagNameTypes[0] + str(x))
		fedEgg = system.tag.getTagValue(tagPath + "/" + tagNameTypes[1] + str(x))
		ds.append({"id": x, "broken": brokenEgg, "waste": (fedEgg - brokenEgg)})

	writer = shared.misc.get_response_servlet(request['servletResponse'])
	writer.println('{"rows":' + str(system.util.jsonEncode(ds)) + '}')
	writer.close
