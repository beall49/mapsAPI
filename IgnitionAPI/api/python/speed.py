	import datetime
	
	min = 60
	returnSize = 100
	
	startTime = datetime.datetime.now() - datetime.timedelta(minutes=int(min))
	endTime = datetime.datetime.now()
	dataSet = system.tag.queryTagHistory(
		paths=["[default]Breaker/Run/Speed"], 
		startDate=startTime, 
		endDate=endTime, 
		aggregationMode="SimpleAverage",
		intervalMinutes=5
		)
	pyDataSet = system.dataset.toPyDataSet(dataSet)
	
	ds= []
	
	for index, row in enumerate(pyDataSet):
		
		ds.append({'id': index, 'TimeStamp':  str(system.date.format(row[0], "hh:mm:s")), 'value': int(row[1])})
			
	response = { 'json': '{"rows":' + system.util.jsonEncode(ds) + '}'}
	return response