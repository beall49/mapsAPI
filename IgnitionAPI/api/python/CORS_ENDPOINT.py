from javax.servlet.http import HttpServletResponse
ds = []
tagNameTypes = "iRowBrokenEggCnt", "iRowFeededEggCnt"
tagPath = "[default]Breaker/PRD"

for x in range(1,13):
	brokenEgg = system.tag.getTagValue(tagPath + "/" +tagNameTypes[0] +str(x))
	fedEgg = system.tag.getTagValue(tagPath + "/" +tagNameTypes[1] +str(x))		
	ds.append({ "id": x, "broken": brokenEgg , "waste": (fedEgg-brokenEgg) })
	
response = system.util.jsonEncode(ds)
HttpServletResponse = request['servletResponse'] 
Response = (HttpServletResponse) 
Response.addHeader('Access-Control-Allow-Origin', '*' )
writer = Response.getWriter()
writer.println(str(response))
writer.close

return None 
