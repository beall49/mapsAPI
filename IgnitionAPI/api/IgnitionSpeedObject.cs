﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgnitionAPI {
    public class IgnitionSpeedObject {
        public List<SpeedRow> rows { get; set; }
    }

    public class SpeedRow {
        public int id { get; set; }
        public int value { get; set; }
        public string TimeStamp { get; set; }
    }
}